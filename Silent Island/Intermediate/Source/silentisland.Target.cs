using UnrealBuildTool;

public class silentislandTarget : TargetRules
{
	public silentislandTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("silentisland");
	}
}
